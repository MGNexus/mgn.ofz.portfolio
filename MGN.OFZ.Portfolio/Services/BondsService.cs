﻿using System.Collections.Generic;
using System.Linq;
using MGN.OFZ.Portfolio.Helpers;
using MGN.OFZ.Portfolio.Interfaces;
using MGN.OFZ.Portfolio.Models.ISS;

namespace MGN.OFZ.Portfolio.Services
{
    public class BondsService: IBondsService
    {
        private readonly IIssIntegration _issIntegration;

        public BondsService(IIssIntegration issIntegration)
        {
            _issIntegration = issIntegration;
        }

        public List<BondItem> GetBonds()
        {
            return new List<BondItem>
            {
                new BondItem {SecId = "test", ShortName = "test"},
                new BondItem {SecId = "test2", ShortName = "test2"},
                new BondItem {SecId = "test", ShortName = "test"},
                new BondItem {SecId = "test2", ShortName = "test2"},
                new BondItem {SecId = "test", ShortName = "test"},
                new BondItem {SecId = "test2", ShortName = "test2"},
                new BondItem {SecId = "test", ShortName = "test"},
                new BondItem {SecId = "test2", ShortName = "test2"},
                new BondItem {SecId = "test", ShortName = "test"},
                new BondItem {SecId = "test2", ShortName = "test2"},
                new BondItem {SecId = "test", ShortName = "test"},
                new BondItem {SecId = "test2", ShortName = "test2"},
                new BondItem {SecId = "test", ShortName = "test"},
                new BondItem {SecId = "test2", ShortName = "test2"},
                new BondItem {SecId = "test3", ShortName = "test3"}
            };
        }

        public BondItem GetBondDetails(string secId)
        {
            var details = AsyncHelpers.RunSync(() => _issIntegration.GetBondDetails(secId));
            var desc = details?.Data?.FirstOrDefault(x => x.Id == "description");

            var item = BondItem.Convert(desc);
            //TODO: save to DB

            return item;
        }
    }
}
