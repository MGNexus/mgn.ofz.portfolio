﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Serialization;
using MGN.OFZ.Portfolio.Interfaces;
using MGN.OFZ.Portfolio.Models.ISS;

namespace MGN.OFZ.Portfolio.Integrations
{
    public class IssIntegration: IIssIntegration
    {
        private const string ISS_URL = "https://iss.moex.com";
        private const int TIMEOUT = 5;

        public async Task<BondDetailsDocument> GetBondDetails(string secId)
        {
            if (string.IsNullOrEmpty(secId))
                throw new ArgumentNullException(nameof(secId));

            using (var client = _getClient())
            {
                var path = $"/iss/securities/{secId}";
                var req = await client.GetAsync(path);
                
                if (req.IsSuccessStatusCode)
                    throw new Exception($"Request by path {ISS_URL}{path} finished with error {req.StatusCode} {req.ReasonPhrase}");

                var serializer = new XmlSerializer(typeof(BondDetailsDocument));

                var contentStream = await req.Content.ReadAsStreamAsync();
                return (BondDetailsDocument)serializer.Deserialize(contentStream);
            }
        }

        private HttpClient _getClient()
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(ISS_URL),
                Timeout = TimeSpan.FromSeconds(TIMEOUT)
            };

            client.DefaultRequestHeaders.Add("Accept", "application/xml");
            client.DefaultRequestHeaders.Add("Content-Type", "application/xml");

            return client;
        }
    }
}
