﻿using MGN.OFZ.Portfolio.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MGN.OFZ.Portfolio.Integrations;
using MGN.OFZ.Portfolio.Services;
using WinRTXamlToolkit.Controls.DataVisualization.Charting;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MGN.OFZ.Portfolio
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            DataContext = new BondsViewModel(new BondsService(new IssIntegration()));

            /*var series = OfzChart.Series[0] as PieSeries;
            if (series == null)
                return;

            series.ItemsSource = ChartData;*/
        }

        /*public void OfzAddButton_Click(object sender, RoutedEventArgs args)
        {
            var name = OfzNameInput.Text;
            var value = int.TryParse(OfzCountInput.Text, out var parsedCount) ? parsedCount : default(int);
            if (value == 0 || string.IsNullOrEmpty(name))
                return;

            ChartData.Add(new ChartData { DataName = name, DataValue = value });
        }*/
    }
}
