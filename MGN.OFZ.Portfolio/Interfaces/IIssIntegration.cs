﻿using System.Threading.Tasks;
using MGN.OFZ.Portfolio.Models.ISS;

namespace MGN.OFZ.Portfolio.Interfaces
{
    public interface IIssIntegration
    {
        Task<BondDetailsDocument> GetBondDetails(string secId);
    }
}
