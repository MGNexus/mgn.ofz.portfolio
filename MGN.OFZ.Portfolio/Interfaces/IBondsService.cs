﻿using System.Collections.Generic;
using MGN.OFZ.Portfolio.Models.ISS;

namespace MGN.OFZ.Portfolio.Interfaces
{
    public interface IBondsService
    {
        List<BondItem> GetBonds();
        BondItem GetBondDetails(string secId);
    }
}
