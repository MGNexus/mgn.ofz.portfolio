﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MGN.OFZ.Portfolio.Annotations;
using MGN.OFZ.Portfolio.Interfaces;
using MGN.OFZ.Portfolio.Models.ISS;

namespace MGN.OFZ.Portfolio.Models
{
    public class BondsViewModel: INotifyPropertyChanged
    {
        private readonly IBondsService _bondsService;

        private List<BondItem> _bonds;
        public List<BondItem> Bonds
        {
            get => _bonds;
            set
            {
                _bonds = value;
                OnPropertyChanged(nameof(Bonds));
            }
        }

        public BondsViewModel(IBondsService bondsService)
        {
            _bondsService = bondsService;
            var bonds = _bondsService.GetBonds();

            Bonds = bonds == null 
                ? new List<BondItem>()
                : new List<BondItem>(bonds);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
