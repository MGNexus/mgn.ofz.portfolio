﻿namespace MGN.OFZ.Portfolio.Models
{
    public class ChartData
    {
        public string DataName { get; set; }
        public int DataValue { get; set; }
    }
}
