﻿using System;

namespace MGN.OFZ.Portfolio.Models.ISS
{
    public class BondItem
    {
        public string SecId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime MaturityDate { get; set; }
        public int InitFaceValue { get; set; }
        public CurrencyEnum Currency { get; set; }
        public int DaysToRedemption { get; set; }
        public int IssueSize { get; set; }
        public int FaceValue { get; set; }
        public int CouponFrequency { get; set; }
        public DateTime CouponDate { get; set; }
        public float CouponValue { get; set; }

        public static BondItem Convert(BondDetailsDocumentData data)
        {
            if (data?.Rows == null || data.Rows.Length == 0)
                return null;

            var item = new BondItem();

            foreach (var row in data.Rows)
            {
                switch (row.Name.ToUpperInvariant())
                {
                    case "SECID": item.SecId = row.Value; break;
                    case "NAME": item.Name = row.Value; break;
                    case "SHORTNAME": item.ShortName = row.Value; break;
                    case "ISSUEDATE": item.IssueDate = DateTime.Parse(row.Value); break;
                    case "MATDATE": item.MaturityDate = DateTime.Parse(row.Value); break;
                    case "INITIALFACEVALUE": item.InitFaceValue = int.Parse(row.Value); break;
                    case "FACEUNIT": item.Currency = (CurrencyEnum)Enum.Parse(typeof(CurrencyEnum), row.Value); break;
                    case "DAYSTOREDEMPTION": item.DaysToRedemption = int.Parse(row.Value); break;
                    case "ISSUESIZE": item.IssueSize = int.Parse(row.Value); break;
                    case "FACEVALUE": item.FaceValue = int.Parse(row.Value); break;
                    case "COUPONFREQUENCY": item.CouponFrequency = int.Parse(row.Value); break;
                    case "COUPONDATE": item.CouponDate = DateTime.Parse(row.Value); break;
                    case "COUPONVALUE": item.CouponValue = float.Parse(row.Value); break;
                }
            }

            return item;
        }
    }
}