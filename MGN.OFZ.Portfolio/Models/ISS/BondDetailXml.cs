﻿using System;
using System.Xml.Serialization;

namespace MGN.OFZ.Portfolio.Models.ISS
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BondDetailsDocument
    {
        [XmlElement("data")]
        public BondDetailsDocumentData[] Data { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public class BondDetailsDocumentData
    {
        [XmlElement("metadata")]
        public BondDetailsDocumentDataMetadata MetaData { get; set; }

        [XmlArrayItem("row", IsNullable = false)]
        public BondDetailsDocumentDataRow[] Rows { get; set; }

        [XmlAttribute("id")]
        public string Id { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public class BondDetailsDocumentDataMetadata
    {

        [XmlArrayItem("column", IsNullable = false)]
        public BondDetailsDocumentDataMetadataColumn[] Columns { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public class BondDetailsDocumentDataMetadataColumn
    {

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("bytes")]
        public ushort Bytes { get; set; }

        [XmlIgnore]
        public bool BytesSpecified { get; set; }

        [XmlAttribute("max_size")]
        public byte MaxSize { get; set; }

        [XmlIgnore]
        public bool MaxSizeSpecified { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public class BondDetailsDocumentDataRow
    {

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("title")]
        public string Title { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("sort_ordder")]
        public ushort SortOrder { get; set; }

        [XmlIgnore]
        public bool SortOrderSpecified { get; set; }

        [XmlAttribute("is_hidden")]
        public byte IsHidden { get; set; }

        [XmlIgnore]
        public bool IsHiddenSpecified { get; set; }

        [XmlAttribute("precision")]
        public string Precision { get; set; }

        [XmlAttribute("secid")]
        public string SecId { get; set; }

        [XmlAttribute("boardid")]
        public string BoardId { get; set; }

        [XmlAttribute("board_group_id")]
        public byte BoardGroupId { get; set; }

        [XmlIgnore]
        public bool BoardGroupIdSpecified { get; set; }

        [XmlAttribute("market_id")]
        public byte MarketId { get; set; }

        [XmlIgnore]
        public bool MarketIdSpecified { get; set; }

        [XmlAttribute("market")]
        public string Market { get; set; }

        [XmlAttribute("engine_id")]
        public byte EngineId { get; set; }

        [XmlIgnore]
        public bool EngineIdSpecified { get; set; }

        [XmlAttribute("engine")]
        public string Engine { get; set; }

        [XmlAttribute("is_trade")]
        public byte IsTraded { get; set; }

        [XmlIgnore]
        public bool IsTradedSpecified { get; set; }

        [XmlAttribute("decimals")]
        public byte Decimals { get; set; }

        [XmlIgnore]
        public bool DecimalsSpecified { get; set; }

        [XmlAttribute("history_from")]
        public string HistoryFrom { get; set; }

        [XmlAttribute("history_till")]
        public string HistoryTill { get; set; }

        [XmlAttribute("listed_from", DataType = "date")]
        public DateTime ListedFrom { get; set; }

        [XmlIgnore]
        public bool ListedFromSpecified { get; set; }

        [XmlAttribute("listed_till", DataType = "date")]
        public DateTime ListedTill { get; set; }

        [XmlIgnore]
        public bool ListedTillSpecified { get; set; }

        [XmlAttribute("is_primary")]
        public byte IsPrimary { get; set; }

        [XmlIgnore]
        public bool IsPrimarySpecified { get; set; }

        [XmlAttribute("currencyid")]
        public string CurrencyId { get; set; }
    }


}
