﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGN.OFZ.Portfolio.Models
{
    public enum CurrencyEnum
    {
        RUR = 10,
        RUB = 20,
        SUR = 30
    }
}
